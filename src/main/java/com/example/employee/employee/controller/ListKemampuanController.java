package com.example.employee.employee.controller;

import com.example.employee.employee.model.ListKemampuan;
import com.example.employee.employee.modelDTO.ListKemampuanDTO;
import com.example.employee.employee.repository.ListKemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ListKemampuan")
public class ListKemampuanController {

    @Autowired
    ListKemampuanRepository listKemampuanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllListKemampuan() {
        List<ListKemampuan> listListKemampuanEntity = listKemampuanRepository.findAll();
        List<ListKemampuanDTO> listListKemampuanDTO = new ArrayList<ListKemampuanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (ListKemampuan listKemampuanEntity : listListKemampuanEntity) {
            ListKemampuanDTO listKemampuanDTO = new ListKemampuanDTO();

            listKemampuanDTO = modelMapper.map(listKemampuanEntity, ListKemampuanDTO.class);

            listListKemampuanDTO.add(listKemampuanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listListKemampuanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getListKemampuanById(@PathVariable(value = "id") final Long listKemampuanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();
        ListKemampuanDTO listKemampuanDTO = new ListKemampuanDTO();

        listKemampuanDTO = modelMapper.map(listKemampuanEntity, ListKemampuanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", listKemampuanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createListKemampuan(@Valid @RequestBody  ListKemampuanDTO listKemampuanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        ListKemampuan listKemampuanEntity = new ListKemampuan();

        listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class );
        listKemampuanRepository.save(listKemampuanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", listKemampuanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateListKemampuan(@PathVariable(value = "id")  Long listKemampuanId,
        @Valid @RequestBody  ListKemampuanDTO listKemampuanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        ListKemampuan listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class);
        
        listKemampuanEntity.setIdListKemampuan(listKemampuanId);
        listKemampuanRepository.save(listKemampuanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", listKemampuanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteListKemampuan(@PathVariable(value = "id") final Long listKemampuanId) {
        
        ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        listKemampuanRepository.delete(listKemampuanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}