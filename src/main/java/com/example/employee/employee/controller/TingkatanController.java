package com.example.employee.employee.controller;

import com.example.employee.employee.model.Tingkatan;
import com.example.employee.employee.modelDTO.TingkatanDTO;
import com.example.employee.employee.repository.TingkatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/tingkatan")
public class TingkatanController {

    @Autowired
    TingkatanRepository tingkatanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllTingkatan() {
        List<Tingkatan> listTingkatanEntity = tingkatanRepository.findAll();
        List<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Tingkatan tingkatanEntity : listTingkatanEntity) {
            TingkatanDTO tingkatanDTO = new TingkatanDTO();

            tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);

            listTingkatanDTO.add(tingkatanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listTingkatanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getTingkatanById(@PathVariable(value = "id") final Long tingkatanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();
        TingkatanDTO tingkatanDTO = new TingkatanDTO();

        tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", tingkatanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createTingkatan(@Valid @RequestBody  TingkatanDTO tingkatanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Tingkatan tingkatanEntity = new Tingkatan();

        tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class );
        tingkatanRepository.save(tingkatanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", tingkatanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateTingkatan(@PathVariable(value = "id")  Long tingkatanId,
        @Valid @RequestBody  TingkatanDTO tingkatanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Tingkatan tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
        
        tingkatanEntity.setIdTingkatan(tingkatanId);
        tingkatanRepository.save(tingkatanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", tingkatanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteTingkatan(@PathVariable(value = "id") final Long tingkatanId) {
        
        Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        tingkatanRepository.delete(tingkatanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}