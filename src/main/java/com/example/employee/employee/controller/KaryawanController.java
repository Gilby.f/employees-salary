package com.example.employee.employee.controller;

import com.example.employee.employee.model.Karyawan;
import com.example.employee.employee.modelDTO.KaryawanDTO;
import com.example.employee.employee.repository.KaryawanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {

    @Autowired
    KaryawanRepository karyawanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllKaryawan() {
        List<Karyawan> listKaryawanEntity = karyawanRepository.findAll();
        List<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Karyawan karyawanEntity : listKaryawanEntity) {
            KaryawanDTO karyawanDTO = new KaryawanDTO();

            karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);

            listKaryawanDTO.add(karyawanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listKaryawanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getKaryawanById(@PathVariable(value = "id") final Long karyawanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();
        KaryawanDTO karyawanDTO = new KaryawanDTO();

        karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", karyawanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createKaryawan(@Valid @RequestBody  KaryawanDTO karyawanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Karyawan karyawanEntity = new Karyawan();

        karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class );
        karyawanRepository.save(karyawanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", karyawanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateKaryawan(@PathVariable(value = "id")  Long karyawanId,
        @Valid @RequestBody  KaryawanDTO karyawanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Karyawan karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
        
        karyawanEntity.setIdKaryawan(karyawanId);
        karyawanRepository.save(karyawanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", karyawanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteKaryawan(@PathVariable(value = "id") final Long karyawanId) {
        
        Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        karyawanRepository.delete(karyawanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}