package com.example.employee.employee.controller;

import com.example.employee.employee.model.ParameterPajak;
import com.example.employee.employee.modelDTO.ParameterPajakDTO;
import com.example.employee.employee.repository.ParameterPajakRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ParameterPajak")
public class ParameterPajakController {

    @Autowired
    ParameterPajakRepository parameterPajakRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllParameterPajak() {
        List<ParameterPajak> listParameterPajakEntity = parameterPajakRepository.findAll();
        List<ParameterPajakDTO> listParameterPajakDTO = new ArrayList<ParameterPajakDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (ParameterPajak parameterPajakEntity : listParameterPajakEntity) {
            ParameterPajakDTO parameterPajakDTO = new ParameterPajakDTO();

            parameterPajakDTO = modelMapper.map(parameterPajakEntity, ParameterPajakDTO.class);

            listParameterPajakDTO.add(parameterPajakDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listParameterPajakDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getParameterPajakById(@PathVariable(value = "id") final Long parameterPajakId) {

        Map<String, Object> result = new HashMap<String, Object>();
        ParameterPajak parameterPajakEntity = parameterPajakRepository.findById(parameterPajakId).get();
        ParameterPajakDTO parameterPajakDTO = new ParameterPajakDTO();

        parameterPajakDTO = modelMapper.map(parameterPajakEntity, ParameterPajakDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", parameterPajakDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createParameterPajak(@Valid @RequestBody  ParameterPajakDTO parameterPajakDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        ParameterPajak parameterPajakEntity = new ParameterPajak();

        parameterPajakEntity = modelMapper.map(parameterPajakDTO, ParameterPajak.class );
        parameterPajakRepository.save(parameterPajakEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", parameterPajakDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateParameterPajak(@PathVariable(value = "id")  Long parameterPajakId,
        @Valid @RequestBody  ParameterPajakDTO parameterPajakDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        ParameterPajak parameterPajakEntity = modelMapper.map(parameterPajakDTO, ParameterPajak.class);
        
        parameterPajakEntity.setIdParamPajak(parameterPajakId);
        parameterPajakRepository.save(parameterPajakEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", parameterPajakDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteParameterPajak(@PathVariable(value = "id") final Long parameterPajakId) {
        
        ParameterPajak parameterPajakEntity = parameterPajakRepository.findById(parameterPajakId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        parameterPajakRepository.delete(parameterPajakEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}