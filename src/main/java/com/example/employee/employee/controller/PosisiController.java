package com.example.employee.employee.controller;

import com.example.employee.employee.model.Posisi;
import com.example.employee.employee.modelDTO.PosisiDTO;
import com.example.employee.employee.repository.PosisiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/posisi")
public class PosisiController {

    @Autowired
    PosisiRepository posisiRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllPosisi() {
        List<Posisi> listPosisiEntity = posisiRepository.findAll();
        List<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Posisi posisiEntity : listPosisiEntity) {
            PosisiDTO posisiDTO = new PosisiDTO();

            posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);

            listPosisiDTO.add(posisiDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listPosisiDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getPosisiById(@PathVariable(value = "id") final Long posisiId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Posisi posisiEntity = posisiRepository.findById(posisiId).get();
        PosisiDTO posisiDTO = new PosisiDTO();

        posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", posisiDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createPosisi(@Valid @RequestBody  PosisiDTO posisiDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Posisi posisiEntity = new Posisi();

        posisiEntity = modelMapper.map(posisiDTO, Posisi.class );
        posisiRepository.save(posisiEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", posisiDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updatePosisi(@PathVariable(value = "id")  Long posisiId,
        @Valid @RequestBody  PosisiDTO posisiDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Posisi posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
        
        posisiEntity.setIdPosisi(posisiId);
        posisiRepository.save(posisiEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", posisiDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deletePosisi(@PathVariable(value = "id") final Long posisiId) {
        
        Posisi posisiEntity = posisiRepository.findById(posisiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        posisiRepository.delete(posisiEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}