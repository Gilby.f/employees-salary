package com.example.employee.employee.controller;

import com.example.employee.employee.model.Kemampuan;
import com.example.employee.employee.modelDTO.KemampuanDTO;
import com.example.employee.employee.repository.KemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/Kemampuan")
public class KemampuanController {

    @Autowired
    KemampuanRepository kemampuanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllKemampuan() {
        List<Kemampuan> listKemampuanEntity = kemampuanRepository.findAll();
        List<KemampuanDTO> listKemampuanDTO = new ArrayList<KemampuanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Kemampuan kemampuanEntity : listKemampuanEntity) {
            KemampuanDTO kemampuanDTO = new KemampuanDTO();

            kemampuanDTO = modelMapper.map(kemampuanEntity, KemampuanDTO.class);

            listKemampuanDTO.add(kemampuanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listKemampuanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getKemampuanById(@PathVariable(value = "id") final Long kemampuanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();
        KemampuanDTO kemampuanDTO = new KemampuanDTO();

        kemampuanDTO = modelMapper.map(kemampuanEntity, KemampuanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", kemampuanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createKemampuan(@Valid @RequestBody  KemampuanDTO kemampuanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Kemampuan kemampuanEntity = new Kemampuan();

        kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class );
        kemampuanRepository.save(kemampuanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", kemampuanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateKemampuan(@PathVariable(value = "id")  Long kemampuanId,
        @Valid @RequestBody  KemampuanDTO kemampuanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Kemampuan kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class);
        
        kemampuanEntity.setIdKemampuan(kemampuanId);
        kemampuanRepository.save(kemampuanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", kemampuanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteKemampuan(@PathVariable(value = "id") final Long kemampuanId) {
        
        Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        kemampuanRepository.delete(kemampuanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}