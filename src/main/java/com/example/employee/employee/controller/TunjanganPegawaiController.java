package com.example.employee.employee.controller;

import com.example.employee.employee.model.TunjanganPegawai;
import com.example.employee.employee.modelDTO.TunjanganPegawaiDTO;
import com.example.employee.employee.repository.TunjanganPegawaiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/tunjanganPegawai")
public class TunjanganPegawaiController {

    @Autowired
    TunjanganPegawaiRepository tunjanganPegawaiRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllTunjanganPegawai() {
        List<TunjanganPegawai> listTunjanganPegawaiEntity = tunjanganPegawaiRepository.findAll();
        List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (TunjanganPegawai tunjanganPegawaiEntity : listTunjanganPegawaiEntity) {
            TunjanganPegawaiDTO tunjanganPegawaiDTO = new TunjanganPegawaiDTO();

            tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);

            listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listTunjanganPegawaiDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getTunjanganPegawaiById(@PathVariable(value = "id") final Long tunjanganPegawaiId) {

        Map<String, Object> result = new HashMap<String, Object>();
        TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
        TunjanganPegawaiDTO tunjanganPegawaiDTO = new TunjanganPegawaiDTO();

        tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", tunjanganPegawaiDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createTunjanganPegawai(@Valid @RequestBody  TunjanganPegawaiDTO tunjanganPegawaiDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        TunjanganPegawai tunjanganPegawaiEntity = new TunjanganPegawai();

        tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class );
        tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", tunjanganPegawaiDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateTunjanganPegawai(@PathVariable(value = "id")  Long tunjanganPegawaiId,
        @Valid @RequestBody  TunjanganPegawaiDTO tunjanganPegawaiDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        TunjanganPegawai tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
        
        tunjanganPegawaiEntity.setIdTunjanganPegawai(tunjanganPegawaiId);
        tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", tunjanganPegawaiDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteTunjanganPegawai(@PathVariable(value = "id") final Long tunjanganPegawaiId) {
        
        TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        tunjanganPegawaiRepository.delete(tunjanganPegawaiEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}