package com.example.employee.employee.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.employee.employee.model.Karyawan;
import com.example.employee.employee.model.Parameter;
import com.example.employee.employee.model.Pendapatan;
import com.example.employee.employee.model.PresentaseGaji;
import com.example.employee.employee.model.TunjanganPegawai;
import com.example.employee.employee.repository.KaryawanRepository;
import com.example.employee.employee.repository.ParameterRepository;
import com.example.employee.employee.repository.PendapatanRepository;
import com.example.employee.employee.repository.PenempatanRepository;
import com.example.employee.employee.repository.PresentaseGajiRepository;
import com.example.employee.employee.repository.TunjanganPegawaiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/calculationPendapatan")
public class CalculatationPendapatanController {
    @Autowired
    PendapatanRepository pendapatanRepository;

    @Autowired
    KaryawanRepository karyawanRepository;

    @Autowired
    PenempatanRepository penempatanRepository;

    @Autowired
    PresentaseGajiRepository presentaseGajiRepository;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    TunjanganPegawaiRepository tunjanganPegawaiRepository;

    ModelMapper modelMapper = new ModelMapper();

    public BigDecimal gajiPokok(BigDecimal umk, BigDecimal percentaseBesaranGaji) {        
        double gajiPokok = umk.doubleValue() * percentaseBesaranGaji.doubleValue();
        BigDecimal gajiPokokValOf = BigDecimal.valueOf(gajiPokok);
        
        return gajiPokokValOf;
    }
    
    @PostMapping("/api/{tanggalgajian}")
    public Map<String, Object> pendapatan(@PathVariable(value = "tanggalgajian") final String sDate1) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<Karyawan> listKaryawan = karyawanRepository.findAll();
        List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
        BigDecimal gajiPokok = null;
        double tunjanganKeluarga = 0;
        double tunjanganTrasport = 0;
        BigDecimal tunjanganPegawai = null;
        double gajiKotor = 0;
        double bpjs = 0;
        double gajiBersih = 0;
        double pPhPerBulan = 0;
        double uangLembur = 0;
        double uangBonus = 0;
        double takeHomePay = 0;
        List<Parameter> listParameter = parameterRepository.findAll();
        List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
        
        for ( Karyawan karyawan : listKaryawan ) {
            BigDecimal umk = karyawan.getPenempatan().getUmkPenempatan();
            BigDecimal percentaseBesaranGaji = null;
            List<Integer> listMasaKerja = new ArrayList<>();
            
            for ( PresentaseGaji percentaseGaji : listPresentaseGaji ) {
                boolean isCheck = percentaseGaji.getIdTingkatan() == karyawan.getTingkatan().getIdTingkatan() && percentaseGaji.getPosisi().getIdPosisi() == karyawan.getPosisi().getIdPosisi() ;
                if (isCheck) {
                    listMasaKerja.add(percentaseGaji.getMasaKerja());
                    percentaseBesaranGaji = percentaseGaji.getBesaranGaji();
                }
            }

            Collections.sort(listMasaKerja);
            int temp = 0;
            
            if(karyawan.getMasaKerja() <= listMasaKerja.get(0)){
                temp = listMasaKerja.get(0);
            }else if(karyawan.getMasaKerja() <= listMasaKerja.get(1)){
                temp = listMasaKerja.get(1);
            }else {
                temp = listMasaKerja.get(2);
            }

            System.out.println(temp);
            
            gajiPokok = gajiPokok(umk, percentaseBesaranGaji);
            
            for (Parameter parameter : listParameter) {
                tunjanganKeluarga = gajiPokok.doubleValue() * parameter.getTKeluarga().doubleValue();
            }
            
            for (Parameter parameter : listParameter) {
                if (karyawan.getPenempatan().getKotaPenempatan().equalsIgnoreCase("jakarta")) {
                    tunjanganTrasport = parameter.getTTransport().doubleValue();    
                } else {
                    tunjanganTrasport = 0;
                }
                
            }
            
            for (TunjanganPegawai pegawai : listTunjanganPegawai) {
                if (karyawan.getPosisi() == pegawai.getPosisi() && karyawan.getTingkatan() == pegawai.getTingkatan()) {
                    tunjanganPegawai = pegawai.getBesaranTujnaganPegawai();
                }
            }
            

            gajiKotor = gajiPokok.doubleValue() + tunjanganKeluarga + tunjanganTrasport + tunjanganPegawai.doubleValue();

            for (Parameter parameter : listParameter) {
                bpjs = gajiPokok.doubleValue() * parameter.getPBpjs().doubleValue();
            }

            gajiBersih = gajiKotor - bpjs - pPhPerBulan;

            takeHomePay = gajiBersih + uangBonus + uangLembur;

            Pendapatan pendapatan = new Pendapatan();
            
            LocalDate myDate = LocalDate.parse(sDate1);
            Date date = Date.from(myDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

            pendapatan.setTanggalGaji(date);
            pendapatan.setKaryawan(karyawan);
            pendapatan.setGajiPokok(gajiPokok);
            pendapatan.setTunjanganKeluarga(BigDecimal.valueOf(tunjanganKeluarga));
            pendapatan.setTunjanganTransport(BigDecimal.valueOf(tunjanganTrasport));
            pendapatan.setTunjanganPegawai(tunjanganPegawai);
            pendapatan.setGajiKotor(BigDecimal.valueOf(gajiKotor));
            pendapatan.setBpjs(BigDecimal.valueOf(bpjs));
            pendapatan.setPphPerbulan(BigDecimal.valueOf(pPhPerBulan));
            pendapatan.setUangBonus(BigDecimal.valueOf(uangBonus));
            pendapatan.setUangLembur(BigDecimal.valueOf(uangLembur));
            pendapatan.setGajiBersih(BigDecimal.valueOf(gajiBersih));
            pendapatan.setTakeHomePay(BigDecimal.valueOf(takeHomePay));

            pendapatanRepository.save(pendapatan);
        }
        result.put("gajiPokok", "gajiPokok");
        return result;
    } 
}