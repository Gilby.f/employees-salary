package com.example.employee.employee.controller;

import com.example.employee.employee.model.Agama;
import com.example.employee.employee.modelDTO.AgamaDTO;
import com.example.employee.employee.repository.AgamaRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/agama")
public class AgamaController {

    @Autowired
    AgamaRepository agamaRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllAgama() {
        List<Agama> listAgamaEntity = agamaRepository.findAll();
        List<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Agama agamaEntity : listAgamaEntity) {
            AgamaDTO agamaDTO = new AgamaDTO();

            agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);

            listAgamaDTO.add(agamaDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listAgamaDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getAgamaById(@PathVariable(value = "id") final Long agamaId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Agama agamaEntity = agamaRepository.findById(agamaId).get();
        AgamaDTO agamaDTO = new AgamaDTO();

        agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", agamaDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createAgama(@Valid @RequestBody  AgamaDTO agamaDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Agama agamaEntity = new Agama();

        agamaEntity = modelMapper.map(agamaDTO, Agama.class );
        agamaRepository.save(agamaEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", agamaDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateAgama(@PathVariable(value = "id")  Long agamaId,
        @Valid @RequestBody  AgamaDTO agamaDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Agama agamaEntity = modelMapper.map(agamaDTO, Agama.class);
        
        agamaEntity.setIdAgama(agamaId);
        agamaRepository.save(agamaEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", agamaDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteAgama(@PathVariable(value = "id") final Long agamaId) {
        
        Agama agamaEntity = agamaRepository.findById(agamaId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        agamaRepository.delete(agamaEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}