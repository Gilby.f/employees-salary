package com.example.employee.employee.controller;

import com.example.employee.employee.model.Pendapatan;
import com.example.employee.employee.modelDTO.PendapatanDTO;
import com.example.employee.employee.repository.PendapatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/pendapatan")
public class PendapatanController {

    @Autowired
    PendapatanRepository pendapatanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllPendapatan() {
        List<Pendapatan> listPendapatanEntity = pendapatanRepository.findAll();
        List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Pendapatan pendapatanEntity : listPendapatanEntity) {
            PendapatanDTO pendapatanDTO = new PendapatanDTO();

            pendapatanDTO = modelMapper.map(pendapatanEntity, PendapatanDTO.class);

            listPendapatanDTO.add(pendapatanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listPendapatanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getPendapatanById(@PathVariable(value = "id") final Long pendapatanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();
        PendapatanDTO pendapatanDTO = new PendapatanDTO();

        pendapatanDTO = modelMapper.map(pendapatanEntity, PendapatanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", pendapatanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createPendapatan(@Valid @RequestBody  PendapatanDTO pendapatanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Pendapatan pendapatanEntity = new Pendapatan();

        pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class );
        pendapatanRepository.save(pendapatanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", pendapatanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updatePendapatan(@PathVariable(value = "id")  Long pendapatanId,
        @Valid @RequestBody  PendapatanDTO pendapatanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Pendapatan pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class);
        
        pendapatanEntity.setIdPendapatan(pendapatanId);
        pendapatanRepository.save(pendapatanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", pendapatanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deletePendapatan(@PathVariable(value = "id") final Long pendapatanId) {
        
        Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        pendapatanRepository.delete(pendapatanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}