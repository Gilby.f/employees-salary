package com.example.employee.employee.controller;

import com.example.employee.employee.model.Parameter;
import com.example.employee.employee.modelDTO.ParameterDTO;
import com.example.employee.employee.repository.ParameterRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/Parameter")
public class ParameterController {

    @Autowired
    ParameterRepository parameterRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllParameter() {
        List<Parameter> listParameterEntity = parameterRepository.findAll();
        List<ParameterDTO> listParameterDTO = new ArrayList<ParameterDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Parameter parameterEntity : listParameterEntity) {
            ParameterDTO parameterDTO = new ParameterDTO();

            parameterDTO = modelMapper.map(parameterEntity, ParameterDTO.class);

            listParameterDTO.add(parameterDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listParameterDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getParameterById(@PathVariable(value = "id") final Long parameterId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Parameter parameterEntity = parameterRepository.findById(parameterId).get();
        ParameterDTO parameterDTO = new ParameterDTO();

        parameterDTO = modelMapper.map(parameterEntity, ParameterDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", parameterDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createParameter(@Valid @RequestBody  ParameterDTO parameterDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Parameter parameterEntity = new Parameter();

        parameterEntity = modelMapper.map(parameterDTO, Parameter.class );
        parameterRepository.save(parameterEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", parameterDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateParameter(@PathVariable(value = "id")  Long parameterId,
        @Valid @RequestBody  ParameterDTO parameterDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Parameter parameterEntity = modelMapper.map(parameterDTO, Parameter.class);
        
        parameterEntity.setIdParam(parameterId);
        parameterRepository.save(parameterEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", parameterDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteParameter(@PathVariable(value = "id") final Long parameterId) {
        
        Parameter parameterEntity = parameterRepository.findById(parameterId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        parameterRepository.delete(parameterEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}