package com.example.employee.employee.controller;

import com.example.employee.employee.model.PresentaseGaji;
import com.example.employee.employee.modelDTO.PresentaseGajiDTO;
import com.example.employee.employee.repository.PresentaseGajiRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/presentaseGaji")
public class PresentaseGajiController {

    @Autowired
    PresentaseGajiRepository presentaseGajiRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllPresentaseGaji() {
        List<PresentaseGaji> listPresentaseGajiEntity = presentaseGajiRepository.findAll();
        List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (PresentaseGaji presentaseGajiEntity : listPresentaseGajiEntity) {
            PresentaseGajiDTO presentaseGajiDTO = new PresentaseGajiDTO();

            presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);

            listPresentaseGajiDTO.add(presentaseGajiDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listPresentaseGajiDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getPresentaseGajiById(@PathVariable(value = "id") final Long presentaseGajiId) {

        Map<String, Object> result = new HashMap<String, Object>();
        PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();
        PresentaseGajiDTO presentaseGajiDTO = new PresentaseGajiDTO();

        presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", presentaseGajiDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createPresentaseGaji(@Valid @RequestBody  PresentaseGajiDTO presentaseGajiDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        PresentaseGaji presentaseGajiEntity = new PresentaseGaji();

        presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class );
        presentaseGajiRepository.save(presentaseGajiEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", presentaseGajiDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updatePresentaseGaji(@PathVariable(value = "id")  Long presentaseGajiId,
        @Valid @RequestBody  PresentaseGajiDTO presentaseGajiDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        PresentaseGaji presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
        
        presentaseGajiEntity.setIdPresentaseGaji(presentaseGajiId);
        presentaseGajiRepository.save(presentaseGajiEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", presentaseGajiDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deletePresentaseGaji(@PathVariable(value = "id") final Long presentaseGajiId) {
        
        PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        presentaseGajiRepository.delete(presentaseGajiEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}