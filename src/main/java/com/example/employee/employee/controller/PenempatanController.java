package com.example.employee.employee.controller;

import com.example.employee.employee.model.Penempatan;
import com.example.employee.employee.modelDTO.PenempatanDTO;
import com.example.employee.employee.repository.PenempatanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/penempatan")
public class PenempatanController {

    @Autowired
    PenempatanRepository penempatanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllPenempatan() {
        List<Penempatan> listPenempatanEntity = penempatanRepository.findAll();
        List<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Penempatan penempatanEntity : listPenempatanEntity) {
            PenempatanDTO penempatanDTO = new PenempatanDTO();

            penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);

            listPenempatanDTO.add(penempatanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listPenempatanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getPenempatanById(@PathVariable(value = "id") final Long penempatanId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();
        PenempatanDTO penempatanDTO = new PenempatanDTO();

        penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", penempatanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createPenempatan(@Valid @RequestBody  PenempatanDTO penempatanDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Penempatan penempatanEntity = new Penempatan();

        penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class );
        penempatanRepository.save(penempatanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", penempatanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updatePenempatan(@PathVariable(value = "id")  Long penempatanId,
        @Valid @RequestBody  PenempatanDTO penempatanDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Penempatan penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
        
        penempatanEntity.setIdPenempatan(penempatanId);
        penempatanRepository.save(penempatanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", penempatanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deletePenempatan(@PathVariable(value = "id") final Long penempatanId) {
        
        Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        penempatanRepository.delete(penempatanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}