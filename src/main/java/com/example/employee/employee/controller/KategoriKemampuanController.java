package com.example.employee.employee.controller;

import com.example.employee.employee.model.KategoriKemampuan;
import com.example.employee.employee.modelDTO.KategoriKemampuanDTO;
import com.example.employee.employee.repository.KategoriKemampuanRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/kategoriKemampuan")
public class KategoriKemampuanController {

    @Autowired
    KategoriKemampuanRepository kategoriKemampuanRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllKategoriKemampuan() {
        List<KategoriKemampuan> listKategoriKemampuanEntity = kategoriKemampuanRepository.findAll();
        List<KategoriKemampuanDTO> listKategoriKemampuanDTO = new ArrayList<KategoriKemampuanDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (KategoriKemampuan kategoriKemampuanEntity : listKategoriKemampuanEntity) {
            KategoriKemampuanDTO kategoriKemampuanDTO = new KategoriKemampuanDTO();

            kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, KategoriKemampuanDTO.class);

            listKategoriKemampuanDTO.add(kategoriKemampuanDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listKategoriKemampuanDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getKategoriKemampuanById(@PathVariable(value = "id") final Long kategoriKemampuanId) {

        final Map<String, Object> result = new HashMap<String, Object>();
        final KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
        KategoriKemampuanDTO kategoriKemampuanDTO = new KategoriKemampuanDTO();

        kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, KategoriKemampuanDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", kategoriKemampuanDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createKategoriKemampuan(@Valid @RequestBody final  KategoriKemampuanDTO kategoriKemampuanDTO) {
        final Map<String,Object> result = new HashMap<String,Object>();
        KategoriKemampuan kategoriKemampuanEntity = new KategoriKemampuan();

        kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class );
        kategoriKemampuanRepository.save(kategoriKemampuanEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", kategoriKemampuanDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateKategoriKemampuan(@PathVariable(value = "id") final  Long kategoriKemampuanId,
        @Valid @RequestBody final  KategoriKemampuanDTO kategoriKemampuanDTO) {
    
        final Map<String,Object> result = new HashMap<String,Object>();
        
        final KategoriKemampuan kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class);
        
        kategoriKemampuanEntity.setIdKategori(kategoriKemampuanId);
        kategoriKemampuanRepository.save(kategoriKemampuanEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", kategoriKemampuanDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteKategoriKemampuan(@PathVariable(value = "id") final Long kategoriKemampuanId) {
        
        final KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
        final Map<String,Object> result = new HashMap<String,Object>();
    
        kategoriKemampuanRepository.delete(kategoriKemampuanEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }

}