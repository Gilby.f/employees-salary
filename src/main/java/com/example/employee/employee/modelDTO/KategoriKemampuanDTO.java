package com.example.employee.employee.modelDTO;

public class KategoriKemampuanDTO {

	private int idKategori;
	private String namaKategori;

	public int getIdKategori() {
		return idKategori;
	}

	public void setIdKategori(int idKategori) {
		this.idKategori = idKategori;
	}

	public String getNamaKategori() {
		return namaKategori;
	}

	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	
}