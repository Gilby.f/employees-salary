package com.example.employee.employee.modelDTO;

public class UserIdDTO{

	private int idUser;
	private String username;
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}