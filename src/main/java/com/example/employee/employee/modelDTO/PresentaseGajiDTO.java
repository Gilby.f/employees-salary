package com.example.employee.employee.modelDTO;

import java.math.BigDecimal;

public class PresentaseGajiDTO {

	private int idPresentaseGaji;
	private PosisiDTO posisiDTO;
	private Integer idTingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;
	public int getIdPresentaseGaji() {
		return idPresentaseGaji;
	}
	public void setIdPresentaseGaji(int idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}
	public PosisiDTO getPosisiDTO() {
		return posisiDTO;
	}
	public void setPosisiDTO(PosisiDTO posisiDTO) {
		this.posisiDTO = posisiDTO;
	}
	public Integer getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(Integer idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public BigDecimal getBesaranGaji() {
		return besaranGaji;
	}
	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
}

