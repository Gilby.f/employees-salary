package com.example.employee.employee.modelDTO;

public class TingkatanDTO {

	private int idTingkatan;
	private String namaTingkatan;
	public int getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(int idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public String getNamaTingkatan() {
		return namaTingkatan;
	}
	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
}
