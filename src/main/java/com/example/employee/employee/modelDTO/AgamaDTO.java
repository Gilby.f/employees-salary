package com.example.employee.employee.modelDTO;

public class AgamaDTO {

	private int idAgama;
	private String namaAgama;
	public int getIdAgama() {
		return idAgama;
	}
	public void setIdAgama(int idAgama) {
		this.idAgama = idAgama;
	}
	public String getNamaAgama() {
		return namaAgama;
	}
	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
}