package com.example.employee.employee.repository;

import com.example.employee.employee.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Long>{
    
}